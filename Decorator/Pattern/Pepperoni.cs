﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    class Pepperoni : Pizza
    {
        public Pepperoni(string name) : base(name)
        {
        }
        public override int GetCost()
        {
            return 15;
        }
    }
}
