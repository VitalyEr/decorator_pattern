﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    class Margherita : Pizza
    {
        public Margherita(string name) : base(name)
        {
        }
        public override int GetCost()
        {
            return 10;
        }
    }
}
