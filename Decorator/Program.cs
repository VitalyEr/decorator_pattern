﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizza pizza = new Margherita("Маргарита");
            CheesePizza cheese = new CheesePizza(pizza);
            TomatoPizza tomato = new TomatoPizza(cheese);
            CheesePizza cheese2 = new CheesePizza(tomato);
            Console.WriteLine(cheese2.Name + " " + cheese2.GetCost());
            Console.ReadKey();
        }
    }
}
